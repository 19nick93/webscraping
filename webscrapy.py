'''import requests
import json
from bs4 import BeautifulSoup

req = requests.get("https://www.midsouthshooterssupply.com/dept/reloading/primers?currentpage=1")

soup = BeautifulSoup(req.content, "html.parser")

res = soup.title.parent.name
    
#print(soup.get_text())
#print(res.prettify())
print(res)
'''
from bs4 import BeautifulSoup
import requests

def get_title(soup):
	
	try:
		
		title = soup.find("span", attrs={"class":'product-id'})

		title_value = title.string
		title_string = title_value.strip()

		# print(type(title))
		# print(type(title_value))
		# print(type(title_string))
		# print()

	except AttributeError:
		title_string = ""	

	return title_string

def get_price(soup):

	try:
		price = soup.find("span", attrs={'class':'price'}).string.strip()

	except AttributeError:
		price = ""	

	return price

def get_brand(soup):

	try:
		brand = soup.find("class", attrs={'class':'catalog-item-brand'}).string.strip()

	except AttributeError:
		brand = ""	

	return brand



if __name__ == '__main__':

	HEADERS = ({'User-Agent':
	            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
	            'Accept-Language': 'en-US, en;q=0.5'})

	# The webpage URL
	URL = "https://www.midsouthshooterssupply.com/dept/reloading/primers?currentpage=1"


	webpage = requests.get(URL, headers=HEADERS)


	soup = BeautifulSoup(webpage.content, "lxml")


	print("Product Title =", get_title(soup))
	print("Product Price =", get_price(soup))
	print()
	print()